################################################################################
ERROR DETEREMINATIN OF MODEL FIT PARAMETERS VIA BOOTSTRAPPING
################################################################################

This matlab package of scripts determines the errors for the joint FRAP and Squish fitting using a bootstrapping ansatz. 

1) Run the matlab script ‘AverageCurves_Publ.m’ 
Input: Dataset of matlab .fig files showing cortical tension and cortex/cytoplasm ratio over time, from cell squishing experiments. 
(That kind of files are output by the script “AnalyseFRAPcurves.m” in project https://gitlab.com/polffgroup/actincortexanalysis/)
Example file: ‘Cell5_Myh9_mKate_SquishingExp_5uM_blebbistatin_cell5Tension_OuterOverInnerRingFluo4a_040719.fig’
Output: ‘ProteinName_Squish.mat’


2) Run ‘AnalysisScriptSummary.m’
Find this matlab script under the project in project https://gitlab.com/polffgroup/actincortexanalysis/)
(- set the parameters as appropriate for this protein)
- put a breakpoint at the end of the script and carry out the command
save('ProteinName_FRAP','CondInd', 'rsquared', 'RecArr_corr')
Output: ‘ProteinName_FRAP.mat‘

3) Run the main script ‘FRAPandSquishfitting_Bootstrapping_Publ.m’ to carry out the data fitting with the bootstrapping ansatz
Input: The files ProteinName_Squish.mat, ProteinName_FRAP.mat 
which store all the curves for the FRAP and Squishing experiments of this protein.

Output: A text file ‘Output_FRAPSquish_Fitting_ProteinName.txt’
With a list of fit parameters in each row obtained from fitting of each individual subset
-	The first four columns store the binding kinetics parameters: 
kon1 (binding rate of cytoplasmic pool), 
kon2 (binding rate of cortical pool), 
koff (unbinding rate from cortex at zero tension), 
alpha (quantifies mechanosensitivity of unbinding),


The set of fit parameters can now used for statistical analysis and error estimation
