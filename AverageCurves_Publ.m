%date: 10.01.2023
%input: matlab figures showing i) the actin cortex tension and ii) the cortex/cytoplasm ratio
%over time during the squishing, example file: 'Cell5_Myh9_mKate_SquishingExp_5uM_blebbistatin_cell5Tension_OuterOverInnerRingFluo4a_040719.fig'
%output: a file 'ProteinName_Squish.mat'

path='YourDataPath/';



filepattern='Cell*ProteinName*Squish*.fig';
inds=strfind(path,'/');
Label=path(inds(end-1)+1:end-1);
count=0; close all;

figure(1); hold off;
tmin=-20; tmax=400; tcomp=200;  tcomp2=200;  cond=1; tcomp3=200; tcomp4=200; % times in seconds

files=dir([path filepattern]);
inds=strfind(filepattern,'*');
TensionBef=NaN(1, length(files));
TensionAft=NaN(1, length(files));
TensionAft2=NaN(1, length(files));
TensionAft3=NaN(1, length(files)); TensionAft4=NaN(1, length(files));
CortexRatioBef=NaN(1, length(files));
CortexRatioAft=NaN(1, length(files));
CortexRatioAft2=NaN(1, length(files));
CortexRatioAft3=NaN(1, length(files)); CortexRatioAft4=NaN(1, length(files));

tinterp=tmin:5:tmax;
yColl=NaN(length(files), length(tinterp));
TensColl=NaN(length(files), tmax-tmin+1);



for i=1:length(files)
    fig=openfig([path, files(i).name]);
    axesObjs = get(fig, 'Children');
    dataObjs = findall(fig, '-property', 'xdata'); %handles to low-level graphics objects in axes
    xdata = get(dataObjs, 'XData');  %data from low-level grahics objects
    ydata = get(dataObjs, 'YData');
    CoarseCortex=ydata{2}; 
    SmoothCortex=ydata{3}; 
    NegLenT=max(find(xdata{1}<0));
    NegLenC=max(find(xdata{2}<0));
    Tension=ydata{1}; TensTimes=xdata{1};
    if length(NegLenC)==0
        NegLenC=4;
        [maxT,ind]=max(Tension);
        NegLenT=ind;
    end
    
    times=xdata{2}; delt=times(2)-times(1);%Variable times should be the times associated with CoarseCortex
    TensionBef(i)=mean(Tension(1:100));
    TensionAft(i)=mean(NegLenT+50);
    TensionAft(i)=mean(NegLenT+200);
    CortexRatioBef(i)=SmoothCortex(NegLenC+1);
    CortexRatioAft(i)=SmoothCortex(round(tcomp/delt)+NegLenC);
    try
        CortexRatioAft2(i)=SmoothCortex(round((tcomp2)/delt)+NegLenC);
    catch
    end
    %array of normalized cortex/cytoplasm ratios during squishing
    yColl(i,:)=interp1(times, CoarseCortex/median(SmoothCortex(round(270/delt+NegLenC+2):round(280/delt+NegLenC+2))), tinterp);%the index range of the normalization region of the curve must be adapted to the data. Typically, a 10-s interval when the intensity ratio reached its final plateau value is chosen. 
    temp=smooth(Tension(NegLenT+tmin:min(NegLenT+tmax,length(Tension))),3);
    %array of tension evolutions during squishing
    TensColl(i,1:length(temp))=temp;
    TensionMaxSmooth(i)=max(temp);
end



save('ProteinName_Squish','tinterp', 'yColl', 'TensColl');
